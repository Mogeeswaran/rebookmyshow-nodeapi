const mongoose = require('./mongo');
const Schema = mongoose.Schema;


const transaction = new Schema(
    {
        mobile: String,
        points: Number
    }
);

module.exports = mongoose.model('transaction', transaction);